# Análise Banco Parceiro Ioasys

Oferta do melhor produto entre Consórcio ou Financiamento de Véículos para clientes, utilizando de dados comportamentais e geolocalização.

**Uma documentação em Archimate foi idealizada, segue em anexo.**

Através dos requisitos e premissas é possível encontrar as seguintes integrações, barreiras sistêmicas e de disponibilidade:

- Sistema de Consórcio: 
1. Disponibilizado em um Mainframe;
2. Não há evidências de um serviço de acesso, podendo ser necessária a construção, caso possível;
3. Risco de dependência de link para disponibilidade e capacidade, não havendo/possibilitando serviço de acesso;
4. Risco de uma única zona prejudicar o desempenho para regiões distantes, além de não ter replicação para disponibilidade;
5. Não há evidências da capacidade atual do sistema, risco de gargalo de processamento e lentidão para aumentar a capacidade;
6. Não há dados suficientes para mensurar complexidade de desenvolvimento; e
7. Oportunidades para solução de nuvem híbrida.	

- Sistema de Financimento:
1. Não atende a premissa de consumo de 1000 TPS;
2. Não há evidências da localização dos servidores; e
3. Oportunidade de implementação de protocolos mais rápidos, como REST ou gRPC;
4. Oportunidade de implementação de filas de processamento; e
5. Oportunidade de escalar o sistema horizontal ou verticalmente. 

- Dados comportamentais/perfil:
1. Não há evidências da do serviço de disponibilização;
2. Necessário verificação da necessidade dos dados armazenados/trafegados;
3. Necessário criptografia adequada para premissa de desempenho;
4. Risco de furto de privacidade e descumprimento da LGPD; e
5. Oportunidade de caching de informações processadas.

- Geolocalização:
1. Dependência da disponibilidade e licença de terceiros; e
2. AWS é um player confiável para gerenciamento do serviço.

- Salesforce:
1. Dependência da disponibilidade e licença de terceiros; e
2. Salesforce é um player de grande porte no ramo.

- Produto final:
1. Melhor oferta depende de todos os serviço estarem disponíveis;
2. Performance e segurança são atributos de qualidade necessários para a arquitetura; e
3. Oportunidade de caching de melhores produtos processado a partir da mudança de uma das entradas.
